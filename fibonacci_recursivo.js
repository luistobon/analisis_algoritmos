var fibonacci_recursivo = function(n){

	if(n == 0){
		return 0;
	}else if(n == 1){
		return 1;
	}else{
		return fibonacci_recursivo(n-1) + fibonacci_recursivo(n-2);
	}
}

if(process.argv.length != 3){
	
	console.log("Introduzca n.")

} else {

	var n = process.argv[2];

	console.log(fibonacci_recursivo(n));

}
