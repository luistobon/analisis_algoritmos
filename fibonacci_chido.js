var _matriz =[
	[1,1],
	[1,0]
]

var multiplica_matriz = function(x, y){
	return [
	[x[0][0]*y[0][0]+x[0][1]*y[1][0], x[0][0]*y[0][1]+x[0][1]*y[1][1]],
	[x[1][0]*y[0][0]+x[1][1]*y[1][0], x[1][0]*y[0][1]+x[1][1]*y[1][1]],
	];
}

var potencia_matriz = function(matriz, n){

	if(n == 1){
		return matriz;
	} else if(n % 2 === 0){

		var resultado = potencia_matriz(matriz, n/2);

		return multiplica_matriz(resultado, resultado);

	} else{

		var resultado = potencia_matriz(matriz, (n-1)/2);

		var cuadrado = multiplica_matriz(resultado, resultado);

		resultado = multiplica_matriz(cuadrado, matriz);

		return resultado;
	}

}

var fibonacci_chido = function(n){

	if(n == 0){
		return 0;
	}else if(n == 1){
		return 1;
	}else{
		return potencia_matriz(_matriz, n)[0][1];
	}
}

if(process.argv.length != 3){
	
	console.log("Introduzca n.")

} else {

	var n = process.argv[2];

	console.log(fibonacci_chido(n));

}
