
var exponenciacion_sencilla = function(base, exponente){
	var r = 1;
	for(var n = 1; n <= exponente; n++){
		r *= base;
	}
	return r;
}

if(process.argv.length != 4){
	
	console.log("Introduzca la base y el exponente.")

} else {

	var base = process.argv[2];
	var potencia = process.argv[3];

	console.log(exponenciacion_sencilla(base, potencia));

}
