var encuentraMayor = function(a){

	if(a.length > 0){

		var mayor = a[0];
		for(var i = 1; i < a.length; i++){

			if(a[i] > mayor){
				mayor = a[i];
			}
		}

		return mayor;

	}

	return null;

};


var counting_sort = function(a){


	var mayor = encuentraMayor(a);

	var c = [];

	var b = new Array(a.length);


	for(var i = 0; i <= mayor; i++){
		c.push(0);
	}


	for(i = 0; i < a.length; i++){
		c[a[i]]++;
	}


	for(i = 1; i < c.length; i++){
		c[i] += c[i-1];
	}

	for(i = a.length-1; i >= 0; i--){
		b[c[a[i]]-1] = a[i];
		c[a[i]]--;
	}


	return b;

}



if(process.argv.length != 3){
	
	console.log("Introduzca un archivo.")

} else {

	var archivo = process.argv[2];


	//var lista = [5,3,8,23,2,7,1,55];
	var utilerias = require('./utilerias.js');
	var lista = utilerias.leerArchivo(archivo);
	
	console.log(counting_sort(lista));


}


