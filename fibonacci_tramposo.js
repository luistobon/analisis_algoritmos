var fibonacci_tramposo = function(n){

	if(n == 0){
		return 0;
	}else if(n == 1){
		return 1;
	}else{
		var ra = (1 + Math.sqrt(5))/2;
		var c = Math.pow(ra,n)/Math.sqrt(5);

		return Math.round(c);
	}
}

if(process.argv.length != 3){
	
	console.log("Introduzca n.")

} else {

	var n = process.argv[2];

	console.log(fibonacci_tramposo(n));

}
