var obtiene_digito = function (a, digito){

	if (digito){

		if(a.toString().length >= digito){
			return parseInt(a.toString().charAt(a.toString().length - digito), 10);
		} else {
			
			return 0;
		}
		
	} else {
		return a;
	}

	
}

var encuentraMayor = function(a, digito){

	if(a.length > 0){

		var mayor = obtiene_digito(a[0], digito);
		
		for(var i = 1; i < a.length; i++){

			if(obtiene_digito(a[i], digito) > mayor){
				
				mayor = obtiene_digito(a[i], digito);

			}
		}

		return mayor;

	}

	return null;

};


var counting_sort = function(a,digito){


	var mayor = encuentraMayor(a, digito);

	var c = [];

	var b = new Array(a.length);


	for(var i = 0; i <= mayor; i++){
		c.push(0);
	}


	for(i = 0; i < a.length; i++){

		c[obtiene_digito(a[i], digito)]++;
	}


	for(i = 1; i < c.length; i++){
		c[i] += c[i-1];
	}



	for(i = a.length-1; i >= 0; i--){
		b[c[obtiene_digito(a[i], digito)]-1] = a[i];
		c[obtiene_digito(a[i], digito)]--;
	}


	return b;

}


var radix_sort = function(a){

	mayor = encuentraMayor(a);

	numero_cifras = mayor.toString().length;



		for(var j = 1; j <= numero_cifras; j++){

				a = counting_sort(a, j)
		
		}


		return a;


};


if(process.argv.length != 3){
	
	console.log("Introduzca un archivo.")

} else {

	var archivo = process.argv[2];


	//var lista = [5,3,8,23,2,7,1,55];
	var utilerias = require('./utilerias.js');
	var lista = utilerias.leerArchivo(archivo);
	
	console.log(radix_sort(lista));


}



/*
456
33
6
557
121


121
33
456
6
557

6
121
33
456
557

6
33
121
456
557

*/
