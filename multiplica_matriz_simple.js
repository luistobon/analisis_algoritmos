var multiplica_matriz_simple = function (a,b,n){

	var m = [];

	for(var i = 0; i < n; i++){

		m[i] = [];

		for(var j = 0; j < n; j++){
			
			m[i][j] = 0;
			
			for(var k = 0; k < n; k++){

				m[i][j] += a[i][k]*b[k][j];

			}

		}

	}

	return m;

}


var a = [
[2,4],
[6,8]
];

var b = [
[1,3],
[5,7]
];

console.log(multiplica_matriz_simple(a,b,2));
