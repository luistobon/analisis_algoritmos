var fibonacci_iterativo = function(n){

	if(n == 0){
		return 0;
	}else if(n == 1){
		return 1;
	}else{
		var a = 0;
		var b = 1;
		var c = 0;

		for(var i = 2; i <= n; i++){
			c = a + b;
			a = b;
			b = c;
		}

		return c;
	}
}

if(process.argv.length != 3){
	
	console.log("Introduzca n.")

} else {

	var n = process.argv[2];

	console.log(fibonacci_iterativo(n));

}
