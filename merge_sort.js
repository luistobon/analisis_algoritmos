var merge = function(a,b){
	var resultado = [];

	while(a.length > 0 || b.length > 0){

		if(a.length > 0 && b.length > 0){
			if(a[0] <= b[0]){
				resultado.push(a.shift());
			} else {
				resultado.push(b.shift());
			}
		} else if(a.length > 0){
			resultado.push(a.shift());
		} else {
			resultado.push(b.shift());
		}


	}

	return resultado;
}

var merge_sort = function(m){

	if(m.length <= 1){
		return m;
	} else{

		var centro = Math.round(m.length/2);
		var izquierda = merge_sort(m.slice(0,centro));
		var derecha = merge_sort(m.slice(centro, m.length));
		return merge(izquierda,derecha);
	}

}


if(process.argv.length != 3){
	
	console.log("Introduzca un archivo.")

} else {

	var archivo = process.argv[2];


	//var lista = [5,3,8,23,2,7,1,55];
	var utilerias = require('./utilerias.js');
	var lista = utilerias.leerArchivo(archivo);
	
	console.log(merge_sort(lista));


}
