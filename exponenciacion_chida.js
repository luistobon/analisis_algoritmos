
var exponenciacion_chida = function(base, exponente){

		if(exponente == 0){
			return 1;
		} else if(exponente == 1){
			return base;
		} else if(exponente % 2 == 0){
			var r = exponenciacion_chida(base, exponente/2);
			return r*r;
		} else{
			var r = exponenciacion_chida(base, (exponente-1)/2);
			return r*r*base;
		}
}



if(process.argv.length != 4){
	
	console.log("Introduzca la base y el exponente.")

} else {

	var base = process.argv[2];
	var potencia = process.argv[3];

	console.log(exponenciacion_chida(base, potencia));

}
