
var  insertion_sort = function(m){

	for(var i = 1; i < m.length; i++){

		for(var j = i; j > 0; j--){

			if(m[j] <=  m[j-1]){
				var r = m[j];
				m[j] = m[j-1];
				m[j-1] = r;
			}
		}
	}

	return m;

}




if(process.argv.length != 3){
	
	console.log("Introduzca un archivo.")

} else {

	var archivo = process.argv[2];


	//var lista = [5,3,8,23,2,7,1,55];
	var utilerias = require('./utilerias.js');
	var lista = utilerias.leerArchivo(archivo);
	
	console.log(insertion_sort(lista));


}
