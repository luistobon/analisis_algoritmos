var leerArchivo = function(a){

	var fs = require('fs');
	var buffer = new String(fs.readFileSync(a));
	var lista = buffer.split(" ");
	for(var i = 0; i < lista.length; i++){
		lista[i] = parseInt(lista[i], 10);
	}

	return lista;

}


exports.leerArchivo = leerArchivo;